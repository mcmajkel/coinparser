# README #

App is deployed to Heroku platform: 
https://spark-exchange-rates-service.herokuapp.com/


# REQUEST #
GET request with parameter *currency* equal to 3-char currency code.

Example:

```
#!java

?currency=USD
```


# RESPONSE #
* Content type: JSON
* If success, response with HTTP code 200. Example:

```
#!javascript

HTTP/1.1 200 OK
Via: 1.1 vegur
Content-Type: application/json
Connection: close
Server: Jetty(9.0.2.v20130417)
Date: Mon, 22 Jun 2015 16:19:12 GMT

{"currencySymbol":"USD","exchangeRate":"3.6799","currencyDescription":"dolar amerykaÅski","fixDate":"2015-06-22"}
```


* If no such currency found, response with HTTP code 400. Example:

```
#!javascript

HTTP/1.1 400 Bad Request
Via: 1.1 vegur
Content-Type: application/json
Connection: close
Server: Jetty(9.0.2.v20130417)
Date: Mon, 22 Jun 2015 16:16:32 GMT

{"message":"No currency with symbol pbc found"}
```

## Parameters: ##
* currencySymbol: as in input (3-char code)
* exchangeRate: value of returned exchange rate
* currencyDescription: full name of returned currency
* fixDate: exchange rates file publish date (by NBP)
* message: if value not found, message with error info is returned 