

import static spark.SparkBase.setPort;
import com.coinparser.ExchangeRateService;
import com.coinparser.controller.ExchangeRateController;


public class Main {

	public static void main(String[] args) {
		//Heroku assigns different ports each time, so it has to be intercepted by jetty
		ProcessBuilder process = new ProcessBuilder();
        Integer port;
        if (process.environment().get("PORT") != null) {
            port = Integer.parseInt(process.environment().get("PORT"));
        } else {
            port = 8080;
        }
        setPort(port);
        //run the exchange rate controller logic with associated service
		new ExchangeRateController(new ExchangeRateService());
	}
}