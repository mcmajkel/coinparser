package com.coinparser.controller;

import static com.coinparser.utils.JsonUtil.json;
import static spark.Spark.*;
import spark.*;

import com.coinparser.ExchangeRateService;
import com.coinparser.model.ExchangeRate;
import com.coinparser.utils.ResponseError;


public class ExchangeRateController {
	public ExchangeRateController(final ExchangeRateService exchangeRateService){
		get("/", (request, response) -> {
			String currencySymbol = "";
			currencySymbol = request.queryParams("currency");
			ExchangeRate er = exchangeRateService.getExchangeRate(currencySymbol);
			if(er==null){
				//If no symbol is found, return error
				response.status(400);
				return new ResponseError("No currency with symbol %s found", currencySymbol);
			}
			else{
				//If symbol is found, return object and success status
				response.status(200);
				return er;
			}
		},json());
		
		//Make sure response is served JSON format
		after((req, res) -> {
			  res.type("application/json");
			});
	}
}
