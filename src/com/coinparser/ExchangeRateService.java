package com.coinparser;

import com.coinparser.model.ExchangeRate;
import com.coinparser.utils.ExchangeRateProvider;

public class ExchangeRateService {
	public ExchangeRate getExchangeRate(String symbol){

		ExchangeRate exchangeRate = null;
		ExchangeRateProvider exchangeRateProvider = new ExchangeRateProvider();
		try {
			exchangeRate = exchangeRateProvider.getExchangeRateFromNBP(symbol);
		} catch (Exception e) {
			e.printStackTrace();
		} 	
		return exchangeRate;
	}
}
