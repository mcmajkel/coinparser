package com.coinparser.utils;

import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;

import com.coinparser.model.ExchangeRate;


public class ExchangeRateProvider
{

	public ExchangeRate getExchangeRateFromNBP(String _symbol) throws Exception
	{
		_symbol = _symbol.toUpperCase();
		URL url = new URL("http://www.nbp.pl/kursy/xml/LastA.xml");
		URLConnection connection = url.openConnection();

		Document doc = parseXML(connection.getInputStream());

		XPathFactory xPathfactory = XPathFactory.newInstance();
		XPath xpath = xPathfactory.newXPath();
		//XPath queries to find nodes with necessary data
		String query_exchangeRate = "//pozycja[kod_waluty='"+_symbol+"']/kurs_sredni";
		String query_currencyDescription = "//pozycja[kod_waluty='"+_symbol+"']/nazwa_waluty";
		String query_publishDate = "//data_publikacji";
		Node exchangeRateNode = (Node) xpath.compile(query_exchangeRate).evaluate(doc, XPathConstants.NODE);
		Node publishDateNode = (Node) xpath.compile(query_publishDate).evaluate(doc, XPathConstants.NODE);
		Node currencyDescriptionNode = (Node) xpath.compile(query_currencyDescription).evaluate(doc, XPathConstants.NODE);

		try {
			String rate = exchangeRateNode.getTextContent();
			String currencyDescription = currencyDescriptionNode.getTextContent();
			String publishDate = publishDateNode.getTextContent();
			ExchangeRate exchangeRate=new ExchangeRate(_symbol, rate, currencyDescription, publishDate);
			return exchangeRate;
		} catch (NullPointerException e) {
			return null;
		}
	}

	private Document parseXML(InputStream stream)
			throws Exception
	{
		DocumentBuilderFactory objDocumentBuilderFactory = null;
		DocumentBuilder objDocumentBuilder = null;
		Document doc = null;
		try
		{
			objDocumentBuilderFactory = DocumentBuilderFactory.newInstance();
			objDocumentBuilder = objDocumentBuilderFactory.newDocumentBuilder();

			doc = objDocumentBuilder.parse(stream);
		}
		catch(Exception ex)
		{
			throw ex;
		}       

		return doc;
	}
}