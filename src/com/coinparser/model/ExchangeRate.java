package com.coinparser.model;

import java.util.Date;

public class ExchangeRate {
	private String currencySymbol;
	private String exchangeRate;
	private String currencyDescription;
	private String fixDate;
	
	
	public String getFixDate() {
		return fixDate;
	}
	public String getCurrencySymbol() {
		return currencySymbol;
	}
	public String getExchangeRate() {
		return exchangeRate;
	}

	public String getCurrencyDescription() {
		return currencyDescription;
	}
	
	public ExchangeRate(String _currencySymbol, String _exchangeRate, String _description, String _fixDate){
		this.currencySymbol = _currencySymbol;
		this.exchangeRate = _exchangeRate.replace(',', '.');
		this.currencyDescription = _description;
		this.fixDate = _fixDate;
	}
	
	public boolean isEmpty(){
		if(currencySymbol.isEmpty() && exchangeRate.isEmpty() && currencyDescription.isEmpty()){
			return true;
		}
		else {
			return false;
		}
	}
}
